This is an R package containing potentially highly volatile and untested code 
for distribution to members of the R User Group at the Forest Research Institute 
of Baden-Wuerttemberg.

Feel free to fork it crediting the LICENSE, but do not expect me to pull.
