#' FVA R User Group
#'
#' Codes and functions written for or in meetings of the FVA R User Group. 
#'
#' @note this is a collection of potentially highly volatile codes and
#' functions used as examples at the R User Group at the Forstliche Versuchs- und
#' Forschungsanstalt Baden-Wuerttemberg.
#' @author Dominik Cullmann, <dominik.cullmann@@forst.bwl.de>
#' @section Version: $Id: 21e3a57157dcd729e5bbed73ed5d84159e8ea554 $
#' @name fvarug-package
#' @aliases fvarug-package
#' @docType package
#' @keywords package
NULL
